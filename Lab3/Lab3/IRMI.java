package Lab3;

import java.rmi.Remote;
import java.rmi.RemoteException;

interface IDateTime extends Remote
{
    String getDate () throws RemoteException;
    String getTime () throws RemoteException;
    boolean stop () throws RemoteException;
}