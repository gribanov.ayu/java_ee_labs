package Lab3;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {
    public static void main(String[] args) {
        try {
            Registry registry = LocateRegistry.getRegistry(2501);
            IDateTime stub = (IDateTime) registry.lookup("localhost");

            System.out.println("Menu:\n" +
                    "1) DATE - get date\n" +
                    "2) TIME - get time\n" +
                    "3) EXIT - shutdown\n");
            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
            while (true) {
                String string = input.readLine();

                if(string.equals("DATE")) {
                    System.out.println("Current date: " + stub.getDate());
                }
                else if (string.equals("TIME")) {
                    System.out.println("Current time: " + stub.getTime());
                }
                else if (string.equals("EXIT")) {
                    stub.stop();
                }
            }
        } catch (Exception e) {
            System.out.println("Server has stopped!\nGoodbye!");
        }
    }
}