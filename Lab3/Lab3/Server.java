package Lab3;

import java.rmi.registry.*;
import java.rmi.server.UnicastRemoteObject;

public class Server implements IDateTime {
    static private Registry registry;

    public static void main(String args[]){
        try{
            Server obj = new Server();
            IDateTime stub = (IDateTime) UnicastRemoteObject.exportObject(obj, 0);

            registry = LocateRegistry.createRegistry(2501);
            registry.bind("localhost", stub);
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    @Override
    public String getDate() {
        return java.time.LocalDate.now().toString();
    }

    @Override
    public String getTime() {
        return java.time.LocalTime.now().toString();
    }

    @Override
    public boolean stop() {
        try {
            UnicastRemoteObject.unexportObject(registry, true);
            registry.unbind("localhost");
            System.out.println("Stopped!");
            System.exit(0);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
