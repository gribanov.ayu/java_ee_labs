package Lab2;

import Lab2.Registration;
import Lab2.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

public class ServerThread extends Thread {
    private final ObjectOutputStream toClient;
    private final BufferedReader fromClient;

    public ServerThread(Socket socket) throws IOException {
        this.toClient = new ObjectOutputStream(socket.getOutputStream());
        this.fromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        start();
    }

    public void run() {
        try {
            try {
                while (true) {
                    String line = fromClient.readLine();

                    if (line.equals("TIME")) {
                        toClient.writeObject(new Date().toString());
                    }
                    else if (line.equals("EXIT"))
                    {
                        System.out.println("User disconnected from server!");
                    }
                    else {
                        System.out.println("User '" + line + "' connected to server!");
                        Registration reg = new Registration(new Date(), line);
                        Server.r_list.add(reg);

                        ArrayList<String> dates = new ArrayList<>();
                        for (Registration i : Server.r_list) {
                            if (i.getUser().toLowerCase().trim().equals(line.toLowerCase().trim())) {
                                dates.add(i.getDate().toString());
                            }
                        }

                        toClient.writeObject(dates);
                    }

                    toClient.flush();
                }
            }
            finally {
                toClient.close();
                fromClient.close();
            }
        }
        catch (Exception ex){}
    }
}
