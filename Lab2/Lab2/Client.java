package Lab2;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

class Client {
    private static boolean EXIT = true;

    private Socket socket;

    private BufferedWriter toServer;
    private ObjectInputStream fromServer;

    public void run() {
        try {
            try {

                socket = new Socket(InetAddress.getByName("localhost"), 2501);
                BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

                toServer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                fromServer = new ObjectInputStream(socket.getInputStream());

                String line = input.readLine();

                toServer.write(line + "\n");
                toServer.flush();

                if (line.equals("TIME"))
                {
                    System.out.println("Current date/time: " +  (String)fromServer.readObject());
                }
                else if (line.equals("EXIT"))
                {
                    System.out.println("Goodbye");
                    EXIT = false;
                }
                else
                {
                    ArrayList<String> regs = (ArrayList<String>) fromServer.readObject();
                    System.out.println("User '" + line +"' logged in on:");
                    for (String i: regs) {
                        System.out.println(i);
                    }
                }
            }
            finally {
                toServer.close();
                fromServer.close();
                socket.close();
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Client client = new Client();
        System.out.println("Menu:\n" +
                "1) Type login to get registration dates\n" +
                "2) Type TIME to get current date/time\n" +
                "3) Type EXIT to close client\n");
        while (EXIT) {
            client.run();
        }
    }
}