package Lab2;

import java.net.*;
import java.util.ArrayList;

class Server {
    public static ArrayList<Registration> r_list = new ArrayList<>();

    public void run() {
        try {
            System.out.println("Server is running!");
            ServerSocket serverSocket = new ServerSocket(2501, 50, InetAddress.getByName("localhost"));

            try {
                while(true) {
                    Socket server = serverSocket.accept();
                    try{
                        new ServerThread(server);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
            finally {
                serverSocket.close();
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Server srv = new Server();
        srv.run();
    }
}
