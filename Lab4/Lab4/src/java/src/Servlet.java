/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package src;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author anon
 */
public class Servlet extends HttpServlet {

    private int sum = 0;
    private ArrayList<String> strings = new ArrayList<String>();
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        
        String str = request.getParameter("string");
        
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            
            out.println("<head>");
            out.println("<title>Lab 4 - Servlet</title>");
            out.println("</head>");
            
            out.println("<body>");
            out.println("<div><h3>Lab 4 - Servlet</h3></div>");
            out.println("<form method = \"GET\" action = \"Servlet\">");
            out.println("<label>Text: </label>");
            out.println("<input name = \"string\" type = \"text\" size = \"80\" maxlength=\"80\">");
            out.println("<input type=\"submit\" value = \"Process!\"></form>");
            out.println("</body>");
            
            out.println("</html>");
            
            out.println(this.checkInput(str));
        }
    }

    private String checkInput(String str) {
        if(str == null || "".equals(str.trim())){
            return "<p>Please enter something in the input.</p>";
        }
        
        try {
            this.sum += Integer.parseInt(str);
            
            return "<p>Integer addition: " + this.sum + "</p>";
        } catch (NumberFormatException e) {
            this.strings.add(str);

            StringBuilder sb = new StringBuilder();
            
            sb.append("<p>Received strings:<p>");
            
            for (String s : this.strings) {
                String tmp = "<p>" + s + "</p>";
                sb.append(tmp);
            }
            return sb.toString();
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Method was not implemented");
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
