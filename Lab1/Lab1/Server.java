package Lab1;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.AbstractMap.SimpleEntry;
import java.util.stream.Collectors;

public class Server implements AutoCloseable {
    static final int DEFAULT_PORT = 2501;
    static final int BUFFER_SIZE = 9000;

    private List<Map.Entry<Integer, Message>> messages;

    private DatagramSocket datagramSocket;
    private DatagramPacket datagramPacket;

    private Server() throws SocketException {
        messages = new ArrayList<>();
        datagramPacket = new DatagramPacket(new byte[BUFFER_SIZE], BUFFER_SIZE);
        datagramSocket = new DatagramSocket(DEFAULT_PORT);
    }

    @Override
    public void close() {
        if (!datagramSocket.isClosed()) {
            datagramSocket.close();
        }
    }

    private void run() throws IOException {
        while (true) {
            datagramSocket.receive(datagramPacket);

            try (ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(datagramPacket.getData());
                 ObjectInput objectInputStream = new ObjectInputStream(byteArrayInputStream))
            {
                Object messageObject = objectInputStream.readObject();
                Message message = new Message((Message)messageObject);
                String response;

                switch (message.get()) {
                    case PING:
                        response = "Server is running";
                        break;

                    case ADD:
                        messages.add(new SimpleEntry<>(datagramPacket.getPort(), message));
                        response = "Message added!";
                        break;

                    case TOP_MESSAGE:
                        int top_skip;
                        if (messages.size() > 10)
                            top_skip = messages.size() - 10;
                        else
                            top_skip = 0;

                        List<String> top_result = messages.stream()
                                .skip(top_skip)
                                .map(m -> m.getValue().toString())
                                .collect(Collectors.toList());

                        if(top_result.size() == 0)
                            response = "No messages received to this server!";
                        else
                            response = "Last " + top_result.size() + " messages on server:\n"
                                + String.join("\n", top_result);
                        break;

                    case CLIENT_MESSAGE:
                        List<String> client_messages = messages.stream()
                                .filter(m -> m.getKey().equals(datagramPacket.getPort()))
                                .map(m -> m.getValue().toString())
                                .collect(Collectors.toList());

                        int client_skip;
                        if (client_messages.size() > 10)
                            client_skip = client_messages.size() - 10;
                        else
                            client_skip = 0;

                        List<String> client_list = client_messages.stream()
                                .skip(client_skip)
                                .collect(Collectors.toList());

                        if(client_list.size() == 0)
                            response = "No messages received from this client!";
                        else
                            response = "This client's last " + client_list.size() + " messages on server:\n"
                                    + String.join("\n", client_list);
                        break;

                    default:
                        throw new Exception("Wrong input! Please check instructions!");
                }

                datagramSocket.send(
                    new DatagramPacket(
                        response.getBytes(),
                        response.length(),
                        datagramPacket.getAddress(),
                        datagramPacket.getPort()));

            } catch (Exception ex) {
                System.err.println(ex.getMessage());
            }
        }
    }

    public static void main(String[] args) throws IOException {
        try (Server server = new Server()) {
            server.run();
        }
    }
}
