package Lab1;

import java.util.Scanner;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.*;

public class Client implements AutoCloseable {
    private DatagramPacket datagramPacket;
    private DatagramSocket datagramSocket;
    private Scanner scanner;

    private Client() throws SocketException {
        datagramSocket = new DatagramSocket();
        scanner = new Scanner(System.in);
    }

    private void send(Command command, String message, InetAddress address) {
        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {

            objectOutputStream.writeObject(
                    new Message(
                            command,
                            message,
                            address,
                            Server.DEFAULT_PORT)
            );
            objectOutputStream.flush();

            datagramSocket.send(
                    new DatagramPacket(
                            byteArrayOutputStream.toByteArray(),
                            byteArrayOutputStream.toByteArray().length,
                            address,
                            Server.DEFAULT_PORT)
            );
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    @Override
    public void close() {
        if (!datagramSocket.isClosed()) {
            datagramSocket.close();
        }
    }

    private void run() {
        System.out.println(
                "Client app menu:\n" +
                        "ADD - add message\n" +
                        "TOP_MESSAGE - last 10 messages from all clients\n" +
                        "CLIENT_MESSAGE - last 10 messages from this client\n" +
                        "PING - check server status\n");

        System.out.flush();

        while (true) {
            System.out.print("Client:\t");
            System.out.flush();

            try {
                String[] input = this.scanner.nextLine().trim().toLowerCase().split(" ", 2);

                if (input.length == 1) {
                    this.send(
                            Command.valueOf(input[0].toUpperCase()),
                            null,
                            InetAddress.getLoopbackAddress());
                }
                else if (input[0].toUpperCase().equals(Command.ADD.toString())) {
                    this.send(
                            Command.ADD,
                            input[1],
                            InetAddress.getLoopbackAddress());
                }
                else {
                    throw new IllegalArgumentException();
                }

                this.datagramPacket = new DatagramPacket(new byte[Server.BUFFER_SIZE], Server.BUFFER_SIZE);
                this.datagramSocket.receive(this.datagramPacket);
                System.out.println("Server:\t" + new String(this.datagramPacket.getData()).trim());
            }
            catch (IllegalArgumentException | IOException e) {
                System.err.println("Wrong input! Please check instructions!");
            }
        }
    }

    public static void main(String[] args) throws SocketException {
        try (Client client = new Client()) {
            client.run();
        }
    }
}
