package Lab1;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Date;

enum Command {
    PING,
    ADD,
    TOP_MESSAGE,
    CLIENT_MESSAGE
}

public class Message implements Serializable {
    private static int last;
    private int id;
    private Command command;
    private String message;
    private Date date;
    private InetAddress address;
    private transient int port;

    Message(Command command,
            String message,
            InetAddress address,
            int port) {
        last = 0;
        this.command = command;
        this.message = message;
        this.date = new Date();
        this.address = address;
        this.port = port;
    }

    Message(Message other) {
        this.id = ++last;
        this.command = other.command;
        this.message = other.message;
        this.date = other.date;
        this.address = other.address;
        this.port = other.port;
    }

    Command get() {
        return command;
    }

    @Override
    public String toString() {
        return "<" + address + ":" + id  + ">-<" + message + ">-<" + date + ">";
    }
}
