package com.example.Lab6;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


class Main {
    public static void task1(Connection connection) throws SQLException {
        System.out.println("\n1) Все регистрационные записи::::>");

        try(ResultSet result = connection.createStatement().executeQuery(Queries.SELECT_ALL_REGISTERED_RECORDS.getQuery())){
            while (result.next()) {
                String users_table_login = result.getString("login");
                String registrations_role = result.getString("role");
                Timestamp registrations_date = result.getTimestamp("date");

                System.out.println(
                        users_table_login + "\t" +
                                registrations_role + "\t" +
                                registrations_date.toString().replace(".0", "") + "\n"
                );
            }
        }
    }

    public static void task2(Connection connection) throws SQLException {
        System.out.println("\n2) Все пользователии, которые входили не ранее заданной даты::::>");

        String datetime = "16.09.2012 10:31:46";
        System.out.println("Пример с: " + datetime);
        Timestamp timestamp = Timestamp.valueOf(
                LocalDateTime.parse(datetime.replace(".", "-"),
                        DateTimeFormatter.ofPattern("dd-MM-uuuu HH:mm:ss"))
        );

        PreparedStatement preparedStatement = connection.prepareStatement(Queries.SELECT_REGISTERED_FROM_DATE.getQuery());
        preparedStatement.setString(1, String.valueOf(timestamp.getTime()));

        try(ResultSet result = preparedStatement.executeQuery()) {
            while (result.next()) {
                System.out.println(result.getString("login"));
            }
        }
    }

    public static void task3(Connection connection) throws SQLException {
        System.out.println("\n3) Пользователей, которые регистрировались с разными ролями::::>");

        try(ResultSet result = connection.createStatement().executeQuery(Queries.SELECT_WITH_DIFFERENT_ROLES.getQuery())) {
            while (result.next()) {
                System.out.println(result.getString("login"));
            }
        }
    }

    public static void task4(Connection connection) throws SQLException {
        System.out.println("\n4) Все даты, в которые регистрировалось не менее двух пользователей::::>");

        try(ResultSet result = connection.createStatement().executeQuery(Queries.SELECT_DAY_WITH_MOST_REGISTRATIONS.getQuery())) {
            while (result.next()) {
                System.out.println(result.getString("date"));
            }
        }
    }

    public static void main(String[] args) throws SQLException {
        try {
            JDBCConnection a = new JDBCConnection();
            a.printDataBase();

            task1(a.getConnection());
            task2(a.getConnection());
            task3(a.getConnection());
            task4(a.getConnection());

            a.closeConnection();
            System.out.println("\nРабота завершилась успешно!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
