package com.example.Lab6;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class JDBCConnection {
    private final String url = "jdbc:sqlite::memory:";

    private final java.sql.Connection connection;

    JDBCConnection() throws SQLException {
        this.connection = DriverManager.getConnection(url);

        try(Statement stmt = this.connection.createStatement())
        {
            stmt.execute(Queries.CREATE_USERS_TABLE.getQuery());
            stmt.execute(Queries.CREATE_REGISTRATIONS_TABLE.getQuery());

            System.out.println("Созданные таблицы:::>");

            try(ResultSet result = this.connection.getMetaData().getTables(null, null, null, null)) {
                while (result.next()) {
                    System.out.println(result.getString("TABLE_NAME"));
                }
            }
        }

        this.processData();
    }

    public void closeConnection() throws SQLException {
        if (this.connection != null) {
            this.connection.close();
        }
    }

    void processData(){
        try(BufferedReader reader = new BufferedReader(new FileReader("src/main/java/com/example/Lab6/data.log"))) {
            String line = reader.readLine();

            while (line != null) {
                if(!line.isEmpty())
                {
                    processLine(line.split(" "));
                }

                line = reader.readLine();
            }
        } catch (Exception e) {
            System.out.println("Ошибка чтения файла!");
        }
    }

    private void processLine(String[] a) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(Queries.INSERT_INTO_USERS_TABLE.getQuery());
        preparedStatement.setString(1, a[0]);
        preparedStatement.executeUpdate();

        preparedStatement = connection.prepareStatement(Queries.GET_ID_FROM_USERS_TABLE.getQuery());
        preparedStatement.setString(1, a[0]);

        try(ResultSet result = preparedStatement.executeQuery()) {
            preparedStatement = connection.prepareStatement(Queries.INSERT_INTO_REGISTRATIONS_TABLE.getQuery());
            preparedStatement.setInt(1, result.getInt("id"));
            preparedStatement.setString(2, a[1]);
            LocalDateTime dateTime =
                    LocalDateTime.parse(a[2].replace(".", "-") + " " + a[3],
                            DateTimeFormatter.ofPattern("dd-MM-uuuu HH:mm:ss"));
            preparedStatement.setTimestamp(3, Timestamp.valueOf(dateTime));
            preparedStatement.executeUpdate();
        }
    }

    public void printDataBase() throws SQLException {
        System.out.println("\nБаза данных::::>");

        System.out.println("USERS");
        try(ResultSet result = this.connection.createStatement().executeQuery(Queries.SELECT_ALL_FROM_USERS_TABLE.getQuery())) {
            while (result.next()) {
                int users_table_id = result.getInt("id");
                String users_table_login = result.getString("login");

                System.out.println(users_table_id + "\t" + users_table_login);
            }

        }

        System.out.println("REGISTRATIONS");
        try(ResultSet result = this.connection.createStatement().executeQuery(Queries.SELECT_ALL_FROM_REGISTRATIONS_TABLE.getQuery())) {
            while (result.next()) {
                int registrations_code = result.getInt("code");
                String registrations_role = result.getString("role");
                Timestamp registrations_date = result.getTimestamp("date");
                int registrations_id = result.getInt("id");

                System.out.println(registrations_code + "\t" + registrations_role +
                        "\t" + registrations_date+ "\t" + registrations_id);
            }
        }
    }

    public Connection getConnection() {
        return this.connection;
    }
}
