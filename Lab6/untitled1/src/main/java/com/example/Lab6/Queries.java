package com.example.Lab6;

enum Queries {
    // create queries
    CREATE_USERS_TABLE("CREATE TABLE IF NOT EXISTS users_table(id INTEGER PRIMARY KEY, login CHAR(8) UNIQUE);"),
    CREATE_REGISTRATIONS_TABLE("CREATE TABLE IF NOT EXISTS registration_table(code INTEGER PRIMARY KEY, " +
            "role VARCHAR(16), date TIMESTAMP, id INTEGER, FOREIGN KEY (id) REFERENCES users_table(id));"),

    // fill tables
    INSERT_INTO_USERS_TABLE("INSERT OR IGNORE INTO Users_table (login) Values (?);"),
    INSERT_INTO_REGISTRATIONS_TABLE("INSERT OR IGNORE INTO Registration_table(id, role, date) VALUES (?, ?, ?);"),
    GET_ID_FROM_USERS_TABLE("SELECT id FROM users_table WHERE login = (?);"),

    // print users
    SELECT_ALL_FROM_USERS_TABLE("SELECT * FROM users_table;"),
    SELECT_ALL_FROM_REGISTRATIONS_TABLE("SELECT * FROM registration_table;"),

    // tasks
    SELECT_ALL_REGISTERED_RECORDS("SELECT * FROM users_table natural join registration_table ORDER BY date;"),
    SELECT_REGISTERED_FROM_DATE("SELECT login FROM registration_table NATURAL JOIN users_table WHERE date >= (?);"),
    SELECT_WITH_DIFFERENT_ROLES("SELECT login FROM registration_table natural join users_table GROUP BY login " +
            "HAVING COUNT(DISTINCT ROLE) > 1;"),
    SELECT_DAY_WITH_MOST_REGISTRATIONS("SELECT DATE FROM (SELECT DATE(DATE / 1000, 'unixepoch') AS DATE " +
            "FROM Registration_table) GROUP BY DATE HAVING COUNT(*) > 1;");

    private final String query;
    Queries(String s) {
        this.query = s;
    }

    public String getQuery() {
        return this.query;
    }
}
