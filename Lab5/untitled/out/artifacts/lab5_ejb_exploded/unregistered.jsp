<%@ page language="java"
         contentType="text/html; charset=UTF-8"
         session="true"
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Unregistered</title>
</head>
<body>
<style>
    .bordered {
        width: 300px;
        height: 150px;
        padding: 20px;
        border: 2px solid darkolivegreen;
    }
</style>
<center>
    <h3>Статус: незарегистрированный пользователь!</h3><br>
    <div class="bordered">
        <form action="LoginServlet" method="get">
            Введите ваше имя: <input type="text" name="user"/><br><br>
            Введите ваш пароль: <input type="password" name="pass"/><br><br>
            <input type="submit" value="Войти в систему!"><br><br><br>
        </form>
    </div>
    <br>
    <div class="bordered">
        <form action="MessageServlet" method="get">
            Введите индекс сообщения: <input type="text" name="idx"/><br><br>
            <input type="submit" value="Вывести сообщение!">
            <%
                String str = (String) session.getAttribute("sentence");
            %>
            <h3> <%= str %></h3>
        </form>
    </div>
    <br>
    <div class="bordered">
        <form action="UsersServlet" method="post">
            Сейчас зарегестрировано:<br><br>
            <input type="submit" value="Показать!"><br><br>
            <%
                String val = (String) session.getAttribute("reg_users");
            %>
            <h3> <%= val %></h3>
        </form>
    </div>
</center>
</body>
</html>