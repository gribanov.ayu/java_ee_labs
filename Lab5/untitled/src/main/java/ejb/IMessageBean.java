package ejb;

import util.MessageException;

import javax.ejb.Local;

@Local
public interface IMessageBean {
    boolean login(String login, String psw);
    String[] getUsers() throws MessageException;
    String getMessage (int index) throws MessageException;
    boolean logout ();
}
