package ejb;

import util.Constants;
import util.MessageException;
import util.User;

import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Singleton(name = "MessageBean")
public class MessageBean implements IMessageBean {

    private static final User[] users = {
            new User("admin", "1234"),
            new User("andrey", "5678"),
            new User("bob", "qwerty")
    };

    private static final String[] messages = {
            "Hello world!",
            "Nice to meet you!",
            "Goodbye!"
    };

    @Inject
    private HttpServletRequest request;

    private static final Map<String, Integer> map = new HashMap<>();

    @Override
    public boolean login(String login, String psw) {
        if(map.containsKey(login)) {
            return true;
        }

        if (login != null || psw != null || !login.isEmpty() || !psw.isEmpty()) {
            for (User user : users) {
                if (user.getUsername().equals(login) && user.getPassword().equals(psw)) {
                    map.put(login, 3);
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public String[] getUsers() throws MessageException {
        String userName = (String)request.getSession().getAttribute(Constants.STRING_NAME.value());
        if(map.containsKey(userName))
        {
            return map.keySet().toArray(new String[0]);
        }
        else
            throw new MessageException(MessageException.ErrMessages.USER_ACCESS_DENIED.error());
    }

    @Override
    public String getMessage(int index) throws MessageException {
        if(index >= 0 && index < messages.length)
        {
            String userName = (String)request.getSession().getAttribute(Constants.STRING_NAME.value());

            if(map.containsKey(userName) && map.get(userName) > 0) {
                map.put(userName, map.get(userName) - 1);
                return messages[index];
            } else {
                return "Срок сессии закончился!";
            }
        }
        else
            throw new MessageException(MessageException.ErrMessages.NO_INDEX_EXCEPTION.error());
    }

    @Override
    public boolean logout() {
        String id = (String) request.getSession().getAttribute(Constants.STRING_NAME.value());
        if(map.containsKey(id)) {
            map.remove(id);
            return true;
        }
        return false;
    }
}
