package util;

public class MessageException extends Exception {
    public enum ErrMessages {
        NO_INDEX_EXCEPTION("Не такого индекса!"),
        USER_ACCESS_DENIED("Вы незарегистрированы!"),
        FUNC_UNAVAILABLE("Функция недоступна!");

        private final String message;

        ErrMessages(String message) {
            this.message = message;
        }

        public String error() {
            return message;
        }
    }

    public MessageException(String message) {
        super(message);
    }
}
