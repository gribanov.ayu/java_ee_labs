package util;

public enum Constants {
    REGISTERED_URL("registered.jsp"),
    NOT_REGISTERED_URL("unregistered.jsp"),

    BOOL_IS_REGISTERED("registered"),
    STRING_NAME("registered_name"),
    STRING_SENTENCE("sentence"),
    STRING_REGISTERED_USER("reg_users");

    private final String val;

    Constants(String val) {
        this.val = val;
    }

    public String value() {
        return val;
    }
}
