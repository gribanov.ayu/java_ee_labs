package servlet;

import ejb.IMessageBean;
import util.Constants;

import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(value = "/LoginServlet", name = "LoginServlet")
public class LoginServlet extends HttpServlet {

    @EJB(beanName = "MessageBean")
    private IMessageBean messageBean;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("user");
        String pass = req.getParameter("pass");

        HttpSession session = req.getSession(true);

        if (messageBean.login(name, pass)) {
            session.setAttribute(Constants.STRING_NAME.value(), name);

            session.setAttribute(Constants.STRING_SENTENCE.value(), "");
            session.setAttribute(Constants.STRING_REGISTERED_USER.value(), "");
            session.setAttribute(Constants.BOOL_IS_REGISTERED.value(), true);

            resp.sendRedirect(Constants.REGISTERED_URL.value());
        } else {
            session.setAttribute(Constants.STRING_NAME.value(), "guest");

            session.setAttribute(Constants.STRING_SENTENCE.value(), "");
            session.setAttribute(Constants.STRING_REGISTERED_USER.value(), "");
            session.setAttribute(Constants.BOOL_IS_REGISTERED.value(), false);

            resp.sendRedirect(Constants.NOT_REGISTERED_URL.value());
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        HttpSession session = req.getSession(true);

        messageBean.logout();

        session.setAttribute(Constants.BOOL_IS_REGISTERED.value(), false);
        session.setAttribute(Constants.STRING_SENTENCE.value(), "До свидания!");
        session.setAttribute(Constants.STRING_REGISTERED_USER.value(), "Спасибо что воспользовались!");

        resp.sendRedirect(Constants.NOT_REGISTERED_URL.value());
    }
}
