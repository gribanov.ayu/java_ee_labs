package servlet;

import ejb.IMessageBean;
import util.Constants;
import util.MessageException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(value = "/UsersServlet", name = "UsersServlet")
public class UsersServlet extends HttpServlet {

    @EJB(beanName = "MessageBean")
    private IMessageBean messageBean;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        if((boolean) session.getAttribute(Constants.BOOL_IS_REGISTERED.value())){
            try {
                String arr = String.join(", ", messageBean.getUsers());

                session.setAttribute(Constants.STRING_REGISTERED_USER.value(), arr);
                response.sendRedirect(Constants.REGISTERED_URL.value());
            } catch (MessageException e) {
                if(e.getMessage().equals(MessageException.ErrMessages.USER_ACCESS_DENIED.error()))
                {
                    session.setAttribute(Constants.STRING_REGISTERED_USER.value(), e.getMessage());
                    response.sendRedirect(Constants.REGISTERED_URL.value());
                }
                else
                {
                    session.setAttribute(Constants.STRING_REGISTERED_USER.value(), MessageException.ErrMessages.FUNC_UNAVAILABLE.error());
                    response.sendRedirect(Constants.NOT_REGISTERED_URL.value());
                }
            }
        } else {
            session.setAttribute(Constants.STRING_REGISTERED_USER.value(), MessageException.ErrMessages.USER_ACCESS_DENIED.error());
            response.sendRedirect(Constants.NOT_REGISTERED_URL.value());
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        session.setAttribute(Constants.STRING_REGISTERED_USER.value(), MessageException.ErrMessages.USER_ACCESS_DENIED.error());
        response.sendRedirect(Constants.NOT_REGISTERED_URL.value());
    }
}
