package servlet;

import ejb.IMessageBean;
import util.Constants;
import util.MessageException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(value = "/MessageServlet", name = "MessageServlet")
public class MessageServlet extends HttpServlet {

    @EJB(beanName = "MessageBean")
    private IMessageBean messageBean;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String index = request.getParameter("idx");
        HttpSession session = request.getSession(true);

        if (isNumeric(index)) {
            int i = Integer.parseInt(index);

            try {
                String message = messageBean.getMessage(i);
                if (message.equals("Срок сессии закончился!")) {
                    messageBean.logout();
                    session.setAttribute(Constants.STRING_SENTENCE.value(), "Вы разрегестрировыны.");
                    session.setAttribute(Constants.STRING_REGISTERED_USER.value(), "Функция недоступна.");
                    response.sendRedirect(Constants.NOT_REGISTERED_URL.value());
                }
                else
                {
                    session.setAttribute(Constants.STRING_SENTENCE.value(), message);
                    response.sendRedirect(Constants.REGISTERED_URL.value());
                }
            } catch (MessageException e) {
                session.setAttribute(Constants.STRING_SENTENCE.value(), e.getMessage());
                response.sendRedirect(Constants.REGISTERED_URL.value());
            }
        }
        else
        {
            session.setAttribute(Constants.STRING_SENTENCE.value(), MessageException.ErrMessages.NO_INDEX_EXCEPTION.error());
            response.sendRedirect(Constants.REGISTERED_URL.value());
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession(true);
        session.setAttribute(Constants.STRING_SENTENCE.value(), MessageException.ErrMessages.USER_ACCESS_DENIED.error());
        resp.sendRedirect(Constants.NOT_REGISTERED_URL.value());
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null || strNum.contains(".")) {
            return false;
        }
        try {
            Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
