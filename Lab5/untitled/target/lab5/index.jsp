<%@ page language="java"
         contentType="text/html; charset=UTF-8"
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
    </head>

    <style>
        .bordered {
            width: 300px;
            height: 150px;
            border: 2px solid darkolivegreen;
            padding: 20px;
        }
    </style>

    <center>
        <h3>Добро пожаловать!</h3><br>
        <div class="bordered">
            <form action="LoginServlet" method="get">
                Введите ваше имя: <input type="text" name="user"/><br><br>
                Введите ваш пароль: <input type="password" name="pass"/><br><br>
                <input type="submit" value="Войти в систему!" width="25"><br>
            </form>
        </div>
    </center>
    </body>
</html>